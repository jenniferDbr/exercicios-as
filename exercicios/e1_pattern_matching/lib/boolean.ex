defmodule Boolean do
  def b_not(true), do: false

  def b_not(false), do: true

  def b_and(bool1,bool2) do
    if (bool1 == false) do false
    else if (bool2 == false) do false
          else true
      end
    end
  end

  def b_or(bool1, bool2) do
    if (bool1 == true) do true
    else if (bool2 == true) do true
        else false
        end
    end
  end
end
