defmodule Effects do
  def print(n) do
    Enum.each(1..n, fn(x) ->
      IO.puts x
      end)
  end

  def even_print(n) do
    Enum.each(1..n, fn(x) ->
        if (rem(x,2) == 0), do: IO.puts x
      end)
  end
end
