defmodule Manipulating do
#***************** FILTER **************************

  defp filterp(acc, list, n) do #función auxiliar
    if ((length list) != 1) do
      if ((hd list) > n) do
        acc = acc -- [(hd list)] #quitamos el elemento que es mayor que n
        filterp(acc, list -- [(hd list)],n) #continuamos examinando
      else filterp(acc, list -- [(hd list)],n)
      end
    else if ((hd list) > n) do   #último elemento por examinar porque tl list con 1 elemento da error
              acc = acc -- [(hd list)]
              acc
         else acc
       end
    end
  end

  def filter(list,n) do
    if (list != []) do filterp(list,list,n)
    else list
    end
  end
#***************** REVERSE **************************
  defp reversep(rev, list) do
    if ((length list) != 1) do
      rev = [(hd list)|rev]
      reversep(rev, (tl list))
    else
      rev = [(hd list)|rev]
      rev
    end
  end

  def reverse(list) do
    if (list != []) do
      reversep([], list)
    else list
    end
  end

#***************** CONCATENATE **************************
  defp join(list1, list2) do
    if (list2 == []) do
      list1
    else #UNIMOS
      if ((length list1) != 1) do
        list2 = [(hd (reverse(list1)))|list2] #El último elemento de l1 lo añadimos en la cabeza de l2
        tl_l1 = tl (reverse(list1))
        join(reverse(tl_l1),list2)
      else
         list2 = [(hd list1)|list2]
         list2
      end
    end
  end

  defp concatenatep(acc, list) do
    if ((length list) != 1) do
      if (is_list (hd list)) do
        acc = join(acc, (hd list)) #acc necesita no estar vacío
        concatenatep(acc, (tl list))
      else  #Elementos que no son lista
        acc = join(acc, [(hd list)])
        concatenatep(acc, (tl list))
      end
    else #Último elemento por examinar
      if (is_list (hd list)) do
        acc = join(acc, (hd list))
        acc
      else
        acc = join(acc, [(hd list)])
        acc
      end
    end
  end

  def concatenate(list) do
    if ((length list) > 1) do
      if (is_list (hd list)) do #añadimos el primer elemento a acc para que no esté vacío
        if ((hd list) != []) do
          concatenatep((hd list), (tl list))
        else
          concatenate(tl list)
        end
      else concatenatep([(hd list)], (tl list))
      end
    else if ((length list) == 1) do #si solo tiene un elemento
            if (is_list (hd list)) do #si es del tipo [[1,2]]
              (hd list)
            else list
            end
        else # si es []
          list
        end
    end
  end

#***************** FLATTEN **************************
  def has_lists(list) do #Función que cuenta cuántas listas hay
    if ((length list) > 1) do
      if (is_list (hd list)) do
        true
      else has_lists((tl list))
      end
    else
      if (list == []) do false
      else
        if (is_list (hd list)) do
          true
        else false
        end
      end
    end
  end

  def flatten (list) do
    if (has_lists(list) == false) do list #si no hay listas
    else
       list = concatenate(list)
       flatten(list)
    end
  end

end
