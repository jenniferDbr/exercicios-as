defmodule Echo do
  def start() do
    pid = spawn(fn -> loop() end)
    Process.register(pid,:echo)
    :ok
  end

  defp loop() do
    receive do
      msg -> IO.puts msg
                loop()
    end
  end

  def print(msg) do
    send(:echo, msg)
    :ok
  end

  def stop() do
    pid = Process.whereis(:echo)
    Process.unregister(:echo)
    :ok

  end
end
