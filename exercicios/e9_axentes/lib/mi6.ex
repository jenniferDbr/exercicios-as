defmodule Mi6 do
  use GenServer

  # https://hexdocs.pm/elixir/GenServer.html


  def fundar() do
    {:ok, server} = GenServer.start_link(__MODULE__, :ok)
    Process.register(server, :mi6)
    :ok
  end

  @impl true
  def init(:ok) do
      {:ok, {Db.new(),[]}}
  end
  @impl true
  def handle_cast({:espiar,axente},{axentes,lista_axentes}) do
    case Db.read(axentes,axente) do
        {:ok, id} -> list = Manipulating.filter((Agent.get(id,&(&1))),(Enum.at(Agent.get(id,&(&1)), 0)))
          Agent.update(axente, fn x -> list end)
          {:noreply,{axentes,lista_axentes}}
        {:error,_} -> {:noreply,{axentes,lista_axentes}}

    end

  end

  @impl true
  def handle_cast({:contrainformar,axente},{axentes,lista_axentes}) do
    case Db.read(axentes,axente) do
      {:ok, id} -> list = (Manipulating.reverse(Agent.get(id,&(&1))))
         Agent.update(axente, fn x-> list end)
         {:noreply,{axentes,lista_axentes}}
      {:error, _} -> {:noreply, {axentes,lista_axentes}}
    end
  end

  @impl true
  def handle_cast({axente,destino},{axentes,lista_axentes}) do
    case Agent.start_link(fn -> Enum.shuffle(Create.create(String.length destino)) end, name: axente) do
      {:error, {:already_started, id }} -> {:noreply, {axentes,lista_axentes}}
      {:ok, id} -> {:noreply, {Db.write(axentes, axente, id),lista_axentes ++ [id]}}
    end
  end



  @impl true
  def handle_call({axente},_from, {axentes,lista_axentes}) do
    case Db.read(axentes,axente) do
        {:ok, id} -> {:reply, Agent.get(id,&(&1)), {axentes,lista_axentes}}
        {:error,:not_found} -> {:reply, :you_are_here_we_are_not, {axentes,lista_axentes}}
    end
  end

  defp stop_agents(axentes) do
    if (length(axentes) != 0) do
        Agent.stop(Enum.at(axentes,0))
        stop_agents(axentes--[Enum.at(axentes,0)])
    end
  end

  @impl true
  def terminate(_razon, {axentes,lista_axentes}) do
    stop_agents(lista_axentes)
    Db.destroy(axentes)
  end



############# CLIENTE #################
  def recrutar(axente, destino) do
    GenServer.cast(:mi6,{axente, destino})
  end

  def asignar_mision(axente,mision) do
    GenServer.cast(:mi6, {mision,axente})
  end

  def consultar_estado(axente) do
    GenServer.call(:mi6,{axente})
  end

  def disolver() do
    GenServer.stop(:mi6,:normal)
  end

end
