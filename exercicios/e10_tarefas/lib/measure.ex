defmodule Measure do
  #crear N=length(lista_de_funcions) tarefas con numero_de_elementos de elementos nas listas
  def run(lista_de_funcions, numero_de_elementos) do
    #CREACIÓN
    {tasks,time} = crear_tasks(lista_de_funcions,numero_de_elementos,[],Time.utc_now())
    lists = obtener_listas(tasks, [], true)
    #EXECUCIÓN
    IO.puts "---------------------------------- ---------"
    IO.puts "| Creación de datos     : #{time}      sec |"
    execucion(lista_de_funcions,lists)
      IO.puts "-------------------------------------------"

  end

  def executar({modulo,funcion}, list) do
    t1 = Time.utc_now()
    apply(modulo,funcion,[list])
    t2 = Time.utc_now()
    Time.diff(t2,t1,:microsecond)/1000000
  end


  def execucion(lista_de_funcions, lists) do
    if (length(lista_de_funcions) != 0) do
      ex_task = Task.async(__MODULE__, :executar, [Enum.at(lista_de_funcions,0), Enum.at(lists,0)])
      try do
        imprimir(Enum.at(lista_de_funcions,0),Task.await(ex_task,10000))
      catch
        :exit, _ -> imprimir(Enum.at(lista_de_funcions,0), "interrompida")
      end

      lista_de_funcions = lista_de_funcions -- [Enum.at(lista_de_funcions,0)]
      lists = lists -- [Enum.at(lists,0)]
      execucion(lista_de_funcions,lists)
    end
  end

  def  obtener_listas(tasks, lists, first) do
    if (length(tasks) != 0) do
      if (first) do
        obtener_listas(tasks -- [Enum.at(tasks,0)], lists ++ [Task.await(Enum.at(tasks,0),1000000)],false)
      else
        list_aux = lists ++ [Task.await(Enum.at(tasks,0),1000000)]
        obtener_listas(tasks -- [Enum.at(tasks,0)], list_aux , false)
      end
    else
      lists
    end
  end

  #Crea una lista de números aleatorios
  def crear_lista(num, rand_list) do
    if (num != 0) do
      crear_lista(num-1, rand_list ++ [:rand.uniform(1000)])
    else
      rand_list
    end
  end

  def imprimir(f, t2) do
    case f do
      {_, :quicksort} ->
                IO.puts "| Sorting:quicksort    : #{t2}      sec |"

      {_, :mergesort} ->
        IO.puts "| Sorting:mergesort   : #{t2}      sec |"
      {_, :reverse} ->
        IO.puts "| Manipulating:reverse    : #{t2}      sec |"
      {_, :flatten} ->
        IO.puts "| Manipulating:flatten   : #{t2}      sec |"
    end
  end

  def crear_lista_de_listas(num) do
    #formato [[a],[b]]
    if (rem(num,2) == 0) do
      [crear_lista(div(num,2),[]),crear_lista(div(num,2),[])]
    else
      [crear_lista(div(num,2),[]),crear_lista((div(num,2)+1),[])]
    end
  end

  def crear_tasks(lista,num, tasks,t1) do
    if (length(lista)!=0) do
      if (Enum.at(lista,0) == {Manipulating, :flatten}) do  #se hai que crear lista de listas
        task = Task.async(__MODULE__, :crear_lista_de_listas, [num])

        crear_tasks(lista -- [Enum.at(lista,0)], num, tasks++[task],t1)

      else  #se basta con crear unha lista simple
        task = Task.async(__MODULE__, :crear_lista, [num,[]])
        crear_tasks(lista -- [Enum.at(lista,0)], num, tasks++[task],t1)
      end
    else
      #devolver lista de tasks creados
      {tasks,Time.diff(Time.utc_now(),t1,:microsecond)/1000000}
    end
  end
end
