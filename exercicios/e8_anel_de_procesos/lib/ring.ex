defmodule Ring do

  def start(n, m, msg) do
    pid = spawn_link(__MODULE__, :set_ring,[n-1,n-1,self()])
    send(pid,{m, msg})
  end

  def set_ring(n, count, first) do
    if (n == count) do
      pid = spawn_link(__MODULE__, :set_ring,[n,count-1,self()]) #Apúntase como o primeiro
      loop(pid)
    else
      if (count == 0) do
        loop(first) #enlázase o último co primeiro
      else
        pid = spawn_link(__MODULE__, :set_ring,[n,count-1,first])
        loop(pid)
      end
    end
  end


  def loop(next) do
    receive do
      :stop -> send(next, :stop) #o proceso acaba
              :ok
     {m, msg} ->
              case m do
                  0 -> send(next, :stop)
                  _ ->if (m-1 == 0) do #Se é o último mensaxe manda parar a o resto dos procesos mais para el
                            send(next, :stop)
                      else send(next, {m-1, msg})
                      end
                      loop(next)
              end
      end
    end
  end
