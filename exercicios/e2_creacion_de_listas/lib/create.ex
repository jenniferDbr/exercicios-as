defmodule Create do

  defp reverse_createp(count,n,list) do
    if (n != 0) do
      list = [count] ++ list
      reverse_createp(count+1,n-1,list)
    else list
    end
  end

  def reverse_create(n) do
    count = 1
    list = [count] ++ []
    if (n == 0) do []
    else reverse_createp(count+1, n-1, list)
    end
  end

  defp createp(count,n,list) do
    if (n != 0) do
      list = list ++ [count]
      createp(count+1,n-1,list)
    else list
    end
  end

  def create(n) do
    count = 1
    list = [count] ++ []
    if (n == 0) do []
    else createp(count+1, n-1, list)
    end
  end
end
