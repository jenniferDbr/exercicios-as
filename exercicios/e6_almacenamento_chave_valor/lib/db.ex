defmodule Db do
  def new() do
    #set para que no haya claves duplicadas
    :ets.new(:user_lookup, [:set, :protected])
  end

  def write(db_ref, key, element) do
      :ets.insert(db_ref, {key, element})
      db_ref
  end

  def delete(db_ref, key) do
      :ets.delete(db_ref, key)
      db_ref
  end

  def read(db_ref, key) do
      case :ets.lookup(db_ref,key) do
        [{key, result}] -> {:ok, result}
        _ -> {:error, :not_found}
      end
  end

  def match(db_ref, element) do
    :ets.select(db_ref,[{{:"$1",element},[],[:"$1"]}])
  end

  def destroy(db_ref) do
    :ets.delete(db_ref)
    :ok
  end
end
