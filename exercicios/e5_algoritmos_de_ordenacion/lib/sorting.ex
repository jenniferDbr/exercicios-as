defmodule Sorting do
#******************* QUICKSORT ***********************
  def quicksort([]), do: []
  def quicksort([hd|tl]) do
   {lesser, greater} = Enum.split_with(tl, &(&1 < hd))
   quicksort(lesser) ++ [hd] ++ quicksort(greater)
  end

#******************* MERGESORT ***********************
  def mergesort(list) when length(list) <= 1 do
   list
  end

  def mergesort(list) do
   middle = length(list) |> div(2)
   { left, right } = Enum.split(list, middle)
   :lists.merge(mergesort(left), mergesort(right))
  end
end
